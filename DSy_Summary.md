# DSy Summary

## Motivation

There are three main reasons why we need distributed systems.

* Scaling
* Location
* Fault-tolerance

### Scaling

| Horizontal Scaling                              | Vertical Scaling                        |
| ----------------------------------------------- | --------------------------------------- |
| + Lower cost with massive scale                 | + Lower cost wit small scale            |
| + Easier to add fault-tolerance                 | + No adaption of software required      |
| + Higher availability                           | + Less complexity                       |
| - Adaption of software may be required          | - HW limit for scaling                  |
| - More complex system, more components involved | - Risk of HW failure causing outage     |
|                                                 | - More difficult to add fault-tolerance |

![image-20220704090525756](DSy_Summary.assets/image-20220704090525756.png)

### Location

Latency is physically bounded by the speed of light.

Improvement in latency only through:

* New protocols that reduce round trips
* Placing the services closer to the user maybe with a content delivery network (CDN)

### Fault tolerance

Any hardware will crash eventually

#### Bit flips in memory

Influenced by:

- Sensitivity of each transistor
- Number of transistors on microchip
- Altitude
- Size of transistor, increases sensitivity but makes smaller targets

Error-Correcting code (ECC) memory is used to correct 1 bitflip and detect 2 bitflips

Bitsquatting: DNS Hijacking by registering domain names with single bit errors

Network link outages happen and redundancy is common tool to combat them

## Categorization

### Distributed system definition

A distributed system is a system whose components are located on different networked computers, which communicate and coordinate their actions by passing messages to one another from any system.

They can be **tightly** coupled, where the processing elements or nodes have access to a common memory or **loosely** coupled if they don't.

They can be **homogeneous** in which all processors are of the same type or **heterogeneous** in which they are different.

They can be **small-scale** with just a few nodes or **large-scale** with many nodes.

The **CAP theorem**, also named **Brewer's theorem**, states that any [distributed data store](https://en.wikipedia.org/wiki/Distributed_data_store) can only provide [two of the following three](https://en.wikipedia.org/wiki/Trilemma) guarantees:

- **Consistency**: Every read receives the most recent write or an error.
- **Availability**: Every request receives a (non-error) response, without the guarantee that it contains the most recent write.
- **Partition tolerance**: The system continues to operate despite an arbitrary number of messages being dropped (or delayed) by the network between nodes.

When a [network partition](https://en.wikipedia.org/wiki/Network_partition) failure happens, it must be decided whether to

- cancel the operation and thus decrease the availability but ensure consistency or to
- proceed with the operation and thus provide availability but risk inconsistency.

|                           | "Controlled" distributed systems                             | "Fully" decentralized systems                                |
| ------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Organisation              | 1 responsible organisation                                   | N responsible organizations                                  |
| Churn                     | low churn                                                    | high churn                                                   |
| Security                  | "Secure environment"                                         | "Hostile environment"                                        |
| Availability              | High availability can be guaranteed                          | unpredictable availability                                   |
| Uniformity                | Can be homogeneous / heterogenous                            | is heterogeneous                                             |
| Consistency               | Leader election (Zookeeper, Paxos, Raft)                     | Weak consistency (DHTs)<br />Nakamoto consensus (Proof of Work)<br />Proof of Stake<br />PBFT |
| Replication principles    | higher availability, higher reliability, higher performance, better scalability, but: requires maintaining consistency in replicas | higher availability, higher reliability, higher performance, better scalability, but: requires maintaining consistency in replicas |
| Examples                  | - Amazon DynamoDB<br />- Client/Server                       | - BitTorrent<br />- Blockchain                               |
| Mechanisms that work well | Consistent hashing (DynamoDB, Cassandra)<br />Master nodes, central coordinator | Consistent hashing (DHTs)<br />Flooding/broadcasting         |
| NAT                       | No issues                                                    | Can be huge problem                                          |

#### Replication principles

More replicas -> higher availability -> higher reliability -> higher performance -> better scalability -> but requires maintaining consistency in replicas

#### Transparency

Distributed system should hide its distributed nature

- Location transparency – users should not be aware of the physical location
- Access transparency - users should access resources in a single, uniform way
- Migration, relocation transparency – users should not be aware, that resource have moved
- Replication transparency – users should not be aware about replicas, it should appear as a single resource
- Concurrent transparency – users should not be aware of other users
- Failure transparency – Always try to hide any failure and recovery of computing entities and resources.
- Security transparency – negotiation of cryptographically secure access of resources must require a minimum of user intervention

#### Fallacies of distributed computing

False assumptions that programmers new to distributed applications often make.

1. The network is reliable
2. Latency is zero
3. Bandwidth is infinite
4. The network is secure
5. Topology doesn't change
6. There is one administrator
7. Transport cost is zero
8. The network is homogeneous

## Consensus

**Definition:** Consensus decision-making is a group decision-making process in which group members develop, and agree to support a decision in the best interest of the whole.

A **Byzantine fault** is a condition of a computer system, particularly distributed systems, where components may fail and there is imperfect information on whether a component has failed.

#### Consensus algorithms

##### Paxos

Paxos guarantees that nodes will only ever choose a single value, but does not guarantee that a value will be chosen if a majority of nodes are unavailable. (Consistent, Partition tolerant)

##### Raft

Reliable, Replicated, Redundant, and Fault-Tolerant.

Leadership election with fault tolerance through expected heartbeats from the leader.

##### vDHT

No locking, no timestamps, every update -> new version, no leader election, works well with churn but not heavy churn

In case of heavy churn, API user needs to resolve:

- Get latest version may return fork
- Abort or resolve (join) manually

1. **get() latest version**, check if all replica peers have latest version, if not wait and try again
2. **put() prepared** with data and short TTL, if status is OK on all replica peers, go ahead, otherwise remove the data and go to step 1 (Values are linked to previous versions with the hash of the data)
3. **put () confirmed**

## Protocols

![image-20220706100717680](DSy_Summary.assets/image-20220706100717680.png)

### TCP

![image-20220706102432434](DSy_Summary.assets/image-20220706102432434.png)

![image-20220706102455394](DSy_Summary.assets/image-20220706102540357.png)

#### Flow control vs congestion control

Flow control is controlled by the receiving side. It ensures that the sender only sends what the receiver can handle.

Congestion control is a method of ensuring that everyone across a network has a "fair" amount of access to network resources, at any given time.

### UDP

![image-20220706103113184](DSy_Summary.assets/image-20220706103113184.png)

### SCTP (Stream Control Transmission Protocol)

- Message-based
- Allows data to be divided into multiple streams
- Syn cookies - SCTP uses a four-way handshake with a signed cookie.
- Multi-homing multiple IP addresses of endpoints
- Not widely used (OpenWRT = not enabled by default, UFW = not supported)
- SCTP used by WebRTC, but tunneled over UDP

### TCP/UDP/SCTP comparison

| TCP                          | UDP                         | SCTP                        |
| ---------------------------- | --------------------------- | --------------------------- |
| Transport layer              | Transport layer             | Transport layer             |
| Connection oriented          | Connection less             | Connection oriented         |
| Reliable transfer            | Unreliable transfer         | Reliable transfer           |
| Streams                      | Messages                    | Messages                    |
| Guaranteed order             | Unordered                   | User can choose             |
| Widely used - HTTP/1, HTTP/2 | Widely used - DNS, HTTP/3   | Not widely used - WebRTC    |
| Flow and congestion control  | No flow/congestion control  | Flow and congestion control |
| Heavyweight                  | Lightweight                 | Heavyweight                 |
| Header size is 20 bytes      | Header size is 8 bytes      | Common header is 12 bytes   |
| Error checking and recovery  | Error checking, no recovery | Error checking and recovery |



### TLS

1. "client hello" lists cryptographic information,
   TLS version, ciphers/keys

2. "server hello" chosen cipher, the session ID,
   random bytes, digital certificate (checked by
   client), optional: "client certificate request"

3. Key exchange using random bytes, now
   server and client can calc secret key

4. "finished" message, encrypted with the
   secret key

TLS 1.2: 

- 2 RTT to send first byte, 3 RTT to receive first byte (excl. TCP connection e)
- 3 RTT to send first byte, 4RTT to receive first byte (incl. TCP connection establishment)

![image-20220706103428739](DSy_Summary.assets/image-20220706103428739.png)

TLS 1.3:

- 1 RTT instead of 2 (exkl. TCP connection establishment), 2 RTT (incl. TCP connection establishment)

  - 1.) Client Hello, Key Share

  - 2.) Server Hello, key Share, Verify Certificate,
    Finished

- 0 RTT (exkl. TCP connection establishment) possible, for previous connections, loosing
  perfect forward secrecy

![image-20220706103620236](DSy_Summary.assets/image-20220706103620236.png)



### HTTP/3, QUIC

![img](DSy_Summary.assets/336px-HTTP-1.1_vs._HTTP-2_vs._HTTP-3_Protocol_Stack.svg.png)

QUIC with HTTP/3 allows for 1RTT to first send byte

![image-20220706111922593](DSy_Summary.assets/image-20220706111922593.png)

### WebSocket

- Full-duplex communication over TCP
- HTTP handshake, then upgrade to communication channel
- Data can be text or binary
- Without TLS -> ws:// | With TLS -> wss://
- Some configuration required on LB and RR

### Custom Protocols

- Can be more efficient but needs more time to develop/test
- Interface description language (IDL) can be used to generate custom protocols -> has more overhead
- IDL examples:
  - ASN1, can be used for data serialisation/reserialisation, used in x.509
  - Avro, remote procedure calls and data serialisation framework, used in Hadoop
  - Protobuf, data serialization system from Google, nearly all inter-machine communication at Google
  - Thrift, RPC framework
  - gRPC, RPC framework uses HTTP/2 for transport

## Tor

- Anonymous internet communication system
- Encrypts all messages incl. header, IP
- Sends data through virtual circuit (3 random
  relays)
  - Guard/relay/exit relay (bridge)
- Tor exit node can see traffic if not using hidden services! -> Use SSL/TLS

## Repositories

One Projects includes ~1 technology, can be split into frontend, backend, service...

![image-20220711101548082](DSy_Summary.assets/image-20220711101548082.png)

### Comparison

| Monorepo                                  | Polyrepo                                     |
| ----------------------------------------- | -------------------------------------------- |
| Tight coupling of projects                | Loose coupling of projects                   |
| Everyone sees all code / commits          | Fine grained access control                  |
| Encourages code sharing withing org       | Encourages code sharing across organisations |
| Scaling: large repos, specialized tooling | Scaling: many projects, special coodrination |



### Monorepo

- One repository for all projects

### Multi-Repo / Polyrepo

- Multiple repositories for a project
- Frontend in a different repository than the backend

## Virtualization

### Container vs VM

| Container                                         | Virtual Machine                   |
| ------------------------------------------------- | --------------------------------- |
| + Reduced size of snapshots 2MB vs 45MB (alpine)  | + App can access all OS resources |
| + Quicker spinning up apps                        | + Live migrations                 |
| + / - Available memory is shared                  | + / - Pre allocates memory        |
| + / - Process-based isolation (share same kernel) | + / - "Full" isolation            |

### Container

**Containerization platforms:** Docker, Podman

**OverlayFS:** 

- The lower directory can be read-only or could be an overlay itself
- The upper directory is normally writable
- The workdir is used to prepare files as they are switched between the layers.

**cgroups (control groups):** linux kernel feature that limits, accounts for and isolates resource usage (CPU, memory, disk I/O, network) of a collection of processes

**Linux network namespaces:** provide isolation of system networking resources

**Docker Compose:** manage multiple containers at once

**Dockerfile:** Used to create container images

**Mulit-stage builds:**
![image-20220711130053645](DSy_Summary.assets/image-20220711130053645.png)

#### Deployment

You can use K8s or docker swarm to deploy multiple containers.



## Load Balancing

Allows horizontal scaling through distribution of workloads across multiple computing resources.

**Advantages:** High availability and reliability of the service and flexibility to add and remove servers according to the demand

They can operate at L2/L3/L4/L7

### Types

**Hardware:**

- Only if you control the datacenter
- Often specialised processors and proprietary software
- E.g. F5, Cisco, loadbalancer.org

**Software:**

- E.g.	 	

  - L2/L3: Seesaw

  - L4: LoadMaster, HAProxy (desc), ZEVENET, Neutrino, Balance (C), Nginx, Gobetween, Traefik

  - L7: Envoy (C++), LoadMaster, HAProxy (C), ZEVENET, Neutrino (Java/Scala), Nginx (C), Traefik (golang), Gobetween (golang), Eureka (Java)

- DNS Load balancing: very easy to setup, static, caching -> no fast changes

**Cloud-base:**

- AWS
  - Application Load Balancer ALB, (L7)
  - Network Load Balancer, (L4)
  - Classic Load Balancer (legacy)
- Google Cloud, (L3, L4, L7)
- Cloudflare (L4, L7)
- DigitalOcean (L4)
- Azure (L4, L7)

### Cross-Origin Resource Sharing (CORS)

For security reasons, browsers restrict cross-origin HTTP requests initiated from scripts (among others).

Solution is to use a reverse proxy -> The client only sees the same origin for the API and the frontend assets.

![image-20220711145511441](DSy_Summary.assets/image-20220711145511441.png)

